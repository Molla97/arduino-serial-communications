#include<SoftwareSerial.h>

char ch;
SoftwareSerial mySerial(10, 11); //RX, TX

void setup() {

  Serial.begin(9600);
  mySerial.begin(9600);
  pinMode(13, OUTPUT);
}

void loop() {
  if(mySerial.available()) {
    ch = mySerial.read();
    Serial.write(ch);

    switch(ch) {

      case '1':
      digitalWrite(13, HIGH);
      break;

      case '0':
      digitalWrite(13, LOW);
      break;
      default:
      break;
    }
  }

  if(Serial.available()) {
    
    mySerial.write(Serial.read());
  }
}
